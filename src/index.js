import React from 'react';
import { render } from 'react-dom';

import Calculator from './components/Screens/Calculator';

if (module.hot) {
  module.hot.accept();
}

const root = document.getElementById('root');
render(<Calculator />, root);
