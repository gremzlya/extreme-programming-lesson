import React from 'react';
import uiController from '../../parts/uiController';

console.warn(
`
===========================================================================
необходимо создать методы в uiController:

setFirst(arg)
setSecond(arg)
setAction(arg)
getResult() <-- должен вернуть результат вычисления
---------------------------------------------------------------------------

uiController должен использовать multiplyController и devisionController
для обработки внесенных данных используя такие методы:

setFirst(arg)
setSecond(arg)
setAction(arg)
count() <-- должен вернуть результат вычисления
===========================================================================
`
)

export default class Calculator extends React.Component {
  constructor() {
    super();

    this.state = {
      first: undefined,
      second: undefined,
      selectedAction: undefined,
      result: 0,
    };

    this.setFirst = this.setFirst.bind(this);
    this.setSecond = this.setSecond.bind(this);
    this.setMultipleAction = this.setMultipleAction.bind(this);
    this.setDevisionAction = this.setDevisionAction.bind(this);
    this.count = this.count.bind(this);
  }

  setFirst(e) {
    this.setState({ first: e.target.value });
    try {
      uiController.setFirst(e.target.value)
    } catch (e) {
      console.error('uiController.setFirst is not defined');
    }
  }

  setSecond(e) {
    this.setState({ second: e.target.value });
    try {
      uiController.setSecond(e.target.value)
    } catch (e) {
      console.error('uiController.setSecond is not defined');
    }
  }

  setMultipleAction() {
    this.setState({ selectedAction: '*' });
    try {
      uiController.setAction('*');
    } catch (e) {
      console.error('uiController.setAction is not defined');
    }
  }

  setDevisionAction() {
    this.setState({ selectedAction: ':' });
    try {
      uiController.setAction(':');
    } catch (e) {
       console.error('uiController.setAction is not defined');
    }
  }

  count() {
    let result;

    try {
      result = uiController.getResult();
    } catch (e) {
       console.error('uiController.getResult is not defined');
    }

    this.setState({ result });
  }

  render() {
    return (
      <div className="row" style={{ marginTop: 50 }}>
        <div className="col-md-6 col-md-offset-3">
          <div className="panel panel-default">
            <div className="panel-body">
              <div className="form-inline">
                <div className="form-group">
                  <input type="text" className="form-control" id="exampleInputEmail3" placeholder="первое число" onChange={this.setFirst} />
                </div>
                {' '}
                <div className="radio">
                  <label>
                    <input type="radio" name="optionsRadios" onClick={this.setMultipleAction} /> умножение
                  </label>
                  {' '}
                  <label>
                    <input type="radio" name="optionsRadios"  onClick={this.setDevisionAction} /> деление
                  </label>
                </div>
                {' '}
                <div className="form-group">
                  <input type="text" className="form-control" id="exampleInputPassword3" placeholder="второе число" onChange={this.setSecond}/>
                </div>
                {' '}
                <button type="" className="btn btn-default" onClick={this.count}>COUNT!</button>
                { `  результат ==> ${this.state.result}` }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
} 

