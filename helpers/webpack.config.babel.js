import path from 'path';
import webpack from 'webpack';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ProgressBarWebpackPlugin from 'progress-bar-webpack-plugin';
import moment from 'moment';

const dirSrc = path.resolve(__dirname, '../src');
const dirStatic = path.resolve(__dirname, '../static');
const dirBuild = path.resolve(__dirname, '../build');

const startTime = new Date();

const buildInfo = () => {
  const packageInfo = require('../package.json');

  const currentTime = new Date();

  return {
    name: packageInfo.name,
    version: packageInfo.version,
    upTime: moment.duration(currentTime - startTime).humanize(),
  };
};

export default {
  devtool: 'eval',
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    path.resolve(dirSrc, 'index.js'),
  ],
  output: {
    path: dirBuild,
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        test: /\.jsx?/,
        include: dirSrc,
        query: {
          presets: [
            ['es2015', { modules: false }],
            'react',
          ],
          plugins: [
            'react-hot-loader/babel',
          ],
        },
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: dirStatic },
    ]),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new ProgressBarWebpackPlugin({
      clear: false,
    }),
  ],
  devServer: {
    hot: true,
    historyApiFallback: true,
    quiet: true,
    setup: (app) => {
      app.get('/__status', (req, res) => res.json(buildInfo()));
    },
  },
  stats: {
    colors: true,
  },
};
